package ch.cern.mwod.to.openshift;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class HelloDBconnectionServlet
 */
@WebServlet("/hello_db")
public class SimpleDBconnectionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SimpleDBconnectionServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	private Connection getConnection(String dbType, String jdbcClass)
			throws IOException, SQLException, ClassNotFoundException {
		Class.forName(jdbcClass);
		Connection conn = null;
		Properties connectionProps = new Properties();
		connectionProps.put("user", System.getenv("USER_" + dbType));
		connectionProps.put("password", System.getenv("PASSWORD_" + dbType));
		conn = DriverManager.getConnection(System.getenv("DB_URL_" + dbType), connectionProps);
		return conn;
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		Connection connection = null;
		try {
			connection = getConnection(request.getParameter("db_type"), request.getParameter("jdbc_class"));
			out.println("URL : " + connection.getMetaData().getURL());
			out.println("DB PRODUCT NAME : " + connection.getMetaData().getDatabaseProductName());
			out.println("DB MAJOR VERSION : " + connection.getMetaData().getDatabaseMajorVersion());
			out.println("DB MINOR VERSION : " + connection.getMetaData().getDatabaseMinorVersion());
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			out.println(e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
					out.println(e);
				}
			}
		}
	}

}

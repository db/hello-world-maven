package ch.cern.mwod.to.openshift;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Servlet implementation class HelloDBconnectionServlet
 */
@WebServlet("/hello_datasource")
public class DatasourceConnectionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

//	@Resource(lookup = "java:/MySqlDS")
//	DataSource mysqlDS;
//	@Resource(lookup = "java:/PostgresDS")
//	DataSource postgresqlDS;
	@Resource(lookup = "java:/HikariOracle")
//	DataSource hikariOracleDS;

//	public DatasourceConnectionServlet() {
//		super();
		// TODO Auto-generated constructor stub
//	}

	private Connection getConnection(String dbType)
			throws IOException, SQLException, ClassNotFoundException, NamingException {
//		if (dbType != null && dbType.equals("postgresql")) {
//			return postgresqlDS.getConnection(System.getenv("OPENSHIFT_POSTGRESQL_DB_USERNAME"), System.getenv("OPENSHIFT_POSTGRESQL_DB_PASSWORD"));
//		} 
//		if (dbType != null && dbType.equals("hikari")) {
//			return hikariOracleDS.getConnection(System.getenv("ORACLE_DB_USERNAME"), System.getenv("ORACLE_DB_PASSWORD"));
//		}
//		return mysqlDS.getConnection(System.getenv("OPENSHIFT_MYSQL_DB_USERNAME"), System.getenv("OPENSHIFT_MYSQL_DB_PASSWORD"));
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		Connection connection = null;
		try {
			connection = getConnection(request.getParameter("db_type"));
			out.println("URL : " + connection.getMetaData().getURL());
			out.println("DB PRODUCT NAME : " + connection.getMetaData().getDatabaseProductName());
			out.println("DB MAJOR VERSION : " + connection.getMetaData().getDatabaseMajorVersion());
			out.println("DB MINOR VERSION : " + connection.getMetaData().getDatabaseMinorVersion());
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			out.println(e);
		} catch (NamingException e) {
			e.printStackTrace();
			out.println(e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
					out.println(e);
				}
			}
		}
	}

}
